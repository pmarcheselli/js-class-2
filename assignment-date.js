// Convert to a Date instance
const assignmentDate = '1/21/2019';
let convertDate = new Date(assignmentDate);
console.log(convertDate);
// output: Mon Jan 21 2019 00:00:00 GMT-0800 (Pacific Standard Time)

// Create dueDate which is 7 days after assignmentDate
const dueDate = new Date(convertDate.setDate(convertDate.getDate() + 7));
console.log(dueDate);
// output: Mon Jan 28 2019 00:00:00 GMT-0800 (Pacific Standard Time)

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log
const year = String(dueDate.getFullYear()), 
    month = String(dueDate.getMonth() + 1), 
    day = String(dueDate.getDate()),
    monthWord = dueDate.toLocaleString('en-us', { month: 'long' });

const timeTag = '<time datetime="'+`${year}-${month.padStart(2,'0')}-${day}`+'">'+`${monthWord } ${day}`+','+`${year}`+ '</time>';
console.log(timeTag);
// output: <time datetime="2019-01-28">January 28,2019</time>