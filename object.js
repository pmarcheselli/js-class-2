// 1. Create an object representation of yourself

const myInfo = {
  firstName: 'Patricia',
  lastName: 'Marcheselli',
 'favorite food': 'french fries',
  mom: {
    firstName: 'Stella',
    lastName: 'Marcheselli',
    'favorite food': 'pasta', 
   },
  dad: {
    firstName: 'Victor',
    lastName: 'Marcheselli',
    'favorite food': 'bbq',
  },
}

// 2. Console log dad's firstName, Mom's favorite food
console.log(myInfo.dad.firstName, myInfo.mom["favorite food"]);
// output: Victor pasta