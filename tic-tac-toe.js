
// Create an array representation of a tic tac toe board

const rowOne = ['-', 'O', '-'],
  rowTwo = ['-', 'X', 'O'],
  rowThree = ['X', '-', 'X'];

  
// After the array is created, O claims the top right square. Update that value. 

rowOne.pop();
// output: "-"
rowOne.push('O');
// output: 3 

// Log the grid to the console.
console.log(rowOne, rowTwo, rowThree);
// output: ["-", "O", "O"], ["-", "X", "O"], ["X", "-", "X"] 